package ictgradschool.industry.controlflow.guessing;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {
        int goal = randomNumber();
        guessTheNumber(goal);

        // TODO Write your code here.
    }

    public int randomNumber() {
        int goal = (int) Math.round((Math.random() * 99) + 1);
        return goal;
    }

    public void guessTheNumber(int goal) {
        int guess = 0;
        theGame:
        while (goal!= guess){ System.out.println("Enter your guess (1-100): ");
        guess = Integer.parseInt(Keyboard.readInput());

        if (guess < goal) {
            System.out.println("Too low, try again");
           // System.out.println("Enter your guess (1-100): ");
           // guess = Integer.parseInt(Keyboard.readInput());
        } else if (guess > goal) {
            System.out.println("Too high, try again");
            //System.out.println("Enter your guess (1-100): ");
            //guess = Integer.parseInt(Keyboard.readInput());
        }
    }
    System.out.println("Perfect!");
    System.out.println("Goodbye");
}
    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
