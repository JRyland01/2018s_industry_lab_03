package ictgradschool.industry.controlflow.coderunner;

/**
 * Please run TestCodeRunner to check your answers
 */
public class CodeRunner {
    /**
     * Q1. Compare two names and if they are the same return "Same name",
     * otherwise if they start with exactly the same letter return "Same
     * first letter", otherwise return "No match".
     *
     * @param firstName
     * @param secondName
     * @return one of three Strings: "Same name", "Same first letter",
     * "No match"
     */
    public String areSameName(String firstName, String secondName) {
        String message = "";
        if (firstName == secondName) {
            message = "Same name";
        } else if (firstName.charAt(0) == secondName.charAt(0)) {
            message = "Same first letter";
        } else {
            message = "No match";
        }
        // TODO write answer to Q1
        return message;
    }
    /** areSameName(String, String) => String **/


    /**
     * Q2. Determine if the given year is a leap year.
     *
     * @param year
     * @return true if the given year is a leap year, false otherwise
     */
    public boolean isALeapYear(int year) {
        boolean leapYear = false;
        if (year % 4 == 0) {
            leapYear = true;
            if ((year % 100 == 0) && (year % 400 != 0)) {
                leapYear = false;
            }
        } else {
            leapYear = false;

        }

        // TODO write answer for Q2
        return leapYear;
    }
    /** isALeapYear(int) => boolean **/


    /**
     * Q3. When given an integer, return an integer that is the reverse (its
     * numbers are in reverse to the input).
     * order.
     *
     * @param number
     * @return the integer with digits in reverse order
     */
    public int reverseInt(int number) {
        int reverseNum = 0;

        for (int i = number; i != 0; i /= 10) {
            reverseNum = reverseNum * 10 + i % 10;
        }

        // TODO write answer for Q3
        return reverseNum;
    }
    /** reverseInt(int) => void **/


    /**
     * Q4. Return the given string in reverse order.
     *
     * @param str
     * @return the String with characters in reverse order
     */
    public String reverseString(String str) {
        String reverseStr = "";
        int length = str.length();
        for (int i = length - 1; i >= 0; i--) {
            reverseStr += str.charAt(i);
        }
//        for (int i = 0; i != str.length(); i++) {
//            reverseStr = reverseStr + str.substring(str.length() - 1 - i, str.length() - i);
//        }
        // TODO write answer for Q4
        return reverseStr;
    }
    /** reverseString(String) => void **/


    /**
     * Q5. Generates the simple multiplication table for the given integer.
     * The resulting table should be 'num' columns wide and 'num' rows tall.
     *
     * @param num
     * @return the multiplication table as a newline separated String
     */
    public String simpleMultiplicationTable(int num) {
        String multiplicationTable = "";
        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= num; j++) {
                multiplicationTable += (i * j) + " ";
            }
            multiplicationTable = multiplicationTable.trim();
            multiplicationTable += '\n';
            // TODO write answer for Q
        }
        return multiplicationTable.trim();
    }
    /** simpleMultiplicationTable(int) => void **/


    /**
     * Q6. Determines the Excel column name of the given column number.
     *
     * @param num
     * @return the column title as a String
     */
    public String convertIntToColTitle(int num) {
        String columnName = "";
        String column = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        //for (int i=1; i <=num;i++) {
        if (num <= 0) {
            columnName = "Input is invalid";
        } else if (num < 27) {
            columnName = column.charAt(num - 1) + "";
        } else if (num % 26 != 0) {
            columnName += column.charAt((num / 26) - 1) + "" + column.charAt((num % 26) - 1);
        }


        //}
        // TODO write answer for Q6
        return columnName;
    }
    /** convertIntToColTitle(int) => void **/


    /**
     * Q7. Determine if the given number is a prime number.
     *
     * @param num
     * @return true is the given number is a prime, false otherwise
     */
    public boolean isPrime(int num) {
        int i;
        int j = num / 2;
        if (num == 0 || num == 1) {
            return false;
        } else {
            for (i = 2; i <= j; i++) {
                if (num % i == 0) {
                    return false;
                }
            }
        }

        // TODO write answer for Q7
        return true;

    }
    /** isPrime(int) => void **/


    /**
     * Q8. Determine if the given integer is a palindrome (ignoring negative
     * sign).
     *
     * @param num
     * @return true is int is palindrome, false otherwise
     */
    public boolean isIntPalindrome(int num) {
        int reverseNum = 0;

        for (int i = num; i != 0; i /= 10) {
            reverseNum = reverseNum * 10 + i % 10;
        }
        if (reverseNum == num) {
            return true;
        }
        // TODO write answer for Q8
        return false;
    }
    /** isIntPalindrom(int) => boolean **/


    /**
     * Q9. Determine if the given string is a palindrome (case folded).
     *
     * @param str
     * @return true if string is palindrome, false otherwise
     */
    public boolean isStringPalindrome(String str) {
        String reverseStr = "";

        int length = str.length();

        for (int i = length - 1; i >= 0; i--) {
            reverseStr += str.charAt(i);
        }
        reverseStr = reverseStr.replaceAll(" ", "");
        str = str.replaceAll(" ", "");
        if (reverseStr.equals(str)) {
            return true;
        }
        // TODO write answer for Q9
        return false;
    }
    /** isStringPalindrome(String) => boolean **/


    /**
     * Q10. Generate a space separated list of all the prime numbers from 2
     * to the highest prime less than or equal to the given integer.
     *
     * @param num
     * @return the primes as a space separated list
     */
    public String printPrimeNumbers(int num) {
        String primesStr = "";
        int i, j, c;
        if (num <=0 || num==1){
            primesStr = "No prime number found";
        }
        for (i = 2; i <= num; i++) {
            c = 0;
            for (j = 1; j <= i; j++) {
                if (i % j == 0) {
                    c++;
                }
            }
            if (c == 2) {
                primesStr += +i + " ";
            }
        }
        //primesStr = "No prime number found";
        return primesStr.trim();
    }

}


